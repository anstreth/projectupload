package com.projectupload.model;

import java.io.Serializable;
import java.util.logging.SimpleFormatter;

/**
 * Created by roman on 26.05.15.
 */
public class TokenLoad implements Serializable{
    private String user;
    private String sessionId;


    public TokenLoad(){}


    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getUser() {
        return user;
    }

    @Override
    public String toString(){
        return String.format("user: %s, sessionId: %s", user, sessionId);
    }
}
