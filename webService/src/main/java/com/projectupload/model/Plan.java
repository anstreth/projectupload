package com.projectupload.model;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by woqpw on 20.05.15.
 */
@Entity
@Table(name="Plans")
public class Plan {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="id")
    private Long id;
    @Column(name="user_id")
    private Long user_id;
    @Column(name = "status",length = 1)
    private boolean status;
    @Column(name="date")
    private String stringDate;
    protected Plan(){}
    public Plan(Long user_id,String stringDate,boolean status){
        this.user_id = user_id;
        this.stringDate = stringDate;
        this.status = status;
    }
    @Override
    public String toString(){
        return String.format("Plan[id='%d', user_id='%d', status = '%s', date = '%s']",id,user_id,status,stringDate);
    }
    public Long getId(){
        return this.id;
    }
}
