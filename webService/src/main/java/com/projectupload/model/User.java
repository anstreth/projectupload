package com.projectupload.model;

import com.projectupload.auth.AbstractUser;

import javax.persistence.*;

/**
 * Created by woqpw on 17.05.15.
 */
@Entity
@Table(name  =  "Users")
public class User implements AbstractUser {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name="username")
    private String username;

    @Column(name="password")
    private String password;
    protected User(){}
    public User(String username,String password) {
        this.username = username;
        this.password = password;
    }
    @Override
    public String toString(){
        return String.format("User[id = '%d', username = '%s', password = '%s']",id,username,password);
    }
    public Long getId(){
        return this.id;
    }
    public String getUsername(){
        return this.username;
    }
    public String getPassword() {
        return this.password;
    }
}