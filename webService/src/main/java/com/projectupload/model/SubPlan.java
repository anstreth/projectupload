package com.projectupload.model;

import javax.persistence.*;

/**
 * Created by woqpw on 20.05.15.
 */
@Entity
@Table(name  =  "Subplans")
public class SubPlan {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    @Column(name = "plan_id")
    private Long plan_id;
    @Column(name="status")
    private boolean status;
    @Column(name="filter")
    private String filter;
    @Column(name = "UUID_after")
    private String UUIDafter;
    @Column(name = "UUID_before")
    private String UUIDbefore;
    protected SubPlan(){}
    public SubPlan(Long plan_id,String filter,String UUID){
        this.plan_id = plan_id;
        this.filter = filter;
        this.UUIDbefore = UUID;
        this.status = true;
    }
    @Override
    public String toString(){
        return String.format("SubPlan[id='%d', plan_id='%d], status = '%s'," +
                " filter = '%s', uuidAfter = '%s', uuidBefore = '%s']",id,plan_id,
                status,filter,UUIDafter,UUIDbefore);
    }
    public boolean getStatus(){
        return this.status;
    }
    public String getUUIDbefore(){
        return this.UUIDbefore;
    }
    public String getUUIDafter(){return this.UUIDafter;}
}
