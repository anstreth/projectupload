package com.projectupload.controllers;

import com.projectupload.auth.AuthManagerImp;
import com.projectupload.auth.UserBunchKeysImp;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by roman on 25.05.15.
 */

@Controller
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    AuthManagerImp authManager;

    @Autowired
    UserBunchKeysImp userBunchKeys;

    private static Logger logger = Logger.getLogger(AuthController.class);
    static {
        logger.setLevel(Level.DEBUG);
    }

    @RequestMapping(value = "/terminatesessions", method = RequestMethod.POST)
    public @ResponseBody void terminateSessions(@CookieValue(value = "token", defaultValue = "null")String token){
        if (token.equals("null")){
            logger.debug("No token cookie, you can't terminate");
        } else {
            if (authManager.authentication(token) && userBunchKeys.keyInBunch(token)){
                logger.debug("token is valid and in bunch key; terminating sessions");
                userBunchKeys.removeAllKey(authManager.getUserContext().getUsername());
            }
        }
    }

    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    public @ResponseBody void logout(@CookieValue(value = "token", defaultValue = "null")String token){
        if (token.equals("null")){
            logger.debug("No token cookie, you can't logout");
        } else {
            if (userBunchKeys.keyInBunch(token)){
                userBunchKeys.removeKey(token);
            }
        }
    }

    @RequestMapping(value = "/logon", method = RequestMethod.GET)
    public void logonGet(
            HttpServletRequest request,
            HttpSession session,
            HttpServletResponse httpServletResponse,
            @RequestParam("user")String username,
            @RequestParam("pwd") String password) throws IOException {
        httpServletResponse.sendRedirect(request.getContextPath()+"/");
        //return "forward:/index.html";
    }

    @RequestMapping(value = "/logon", method = RequestMethod.POST)
    public void logon(
            RedirectAttributes redirectAttributes,
            HttpServletRequest request,
            HttpSession session,
            HttpServletResponse httpServletResponse,
            @RequestParam("user")String username,
            @RequestParam("pwd") String password) throws ServletException, IOException {
        logger.debug(String.format("auth controller works with username \"%s\"", username));
        //request.getRequestDispatcher(request.getContextPath() + "/index.html").include(request, httpServletResponse);
//        httpServletResponse.setHeader("Location", "index.html");
//        request.getRequestDispatcher("/index.html").include(request, httpServletResponse);

        logger.debug(httpServletResponse.getContentType());
        httpServletResponse.setContentType("text/html");
        logger.debug(httpServletResponse.getContentType());
        //httpServletResponse.setStatus(httpServletResponse.SC_MOVED_PERMANENTLY);
//        httpServletResponse.setHeader("Location", request.getContextPath());
        //httpServletResponse.sendRedirect(request.getContextPath() + "/index.html");
        request.setAttribute("user", username);
        request.setAttribute("pwd", password);

        redirectAttributes.addAttribute("user", username)
                .addAttribute("pwd", password)
                .addFlashAttribute("flash", "flash");

        if (authManager.authentication(username, password)) {
            logger.debug("correct username and password, adding JWT-cookie and redirecting to filter");
            Cookie tokenCookie = new Cookie("token", authManager.authorization(username, password));
            tokenCookie.setPath("/");
            httpServletResponse.addCookie(tokenCookie);
            userBunchKeys.addKey(username, authManager.getTokenSessionId(tokenCookie.getValue()), tokenCookie.getValue());

        } else {
            logger.debug("incorrect username or password; redirecting back to filter...");
        }

        httpServletResponse.sendRedirect(request.getContextPath()+"/");
        //request.getRequestDispatcher("/index.html").forward(request, httpServletResponse);
    }
}