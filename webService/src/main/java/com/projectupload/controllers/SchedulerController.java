package com.projectupload.controllers;

import org.springframework.core.io.Resource;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by woqpw on 12.05.15.
 */
public interface SchedulerController {
    void getData(@RequestParam("uuid") String uuid,@RequestParam("filter") String filter);
    void closeOperation(HttpServletRequest request,HttpServletResponse response);
    void createZip(Long plan_id);
    void downloadZip(String filename,HttpServletResponse response);

}
