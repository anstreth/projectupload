package com.projectupload.controllers;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

import com.projectupload.auth.AuthManager;
import com.projectupload.auth.AuthManagerImp;
import com.projectupload.model.Plan;
import com.projectupload.model.SubPlan;
import com.projectupload.model.User;
import com.projectupload.scheduler.SchedulerManager;
import com.projectupload.scheduler.impl.SchedulerManagerImp;
import com.projectupload.services.impl.FileMetaServiceImpl;
import com.projectupload.services.impl.PlanServiceImpl;
import com.projectupload.services.impl.SubPlanServiceImpl;
import com.projectupload.services.impl.UserServiceImpl;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.projectupload.model.FileMeta;

@Controller
@RequestMapping("/controller")
public class FileController {
    LinkedList<FileMeta> files = new LinkedList<FileMeta>();

//    @Autowired
//    ConnectionMongo mongo;

    Logger  logger = Logger.getLogger("com.foo");

    @Autowired
    private FileMetaServiceImpl fileMetaService;
    @Autowired
    private SchedulerManagerImp schedulerManager;
    @Autowired
    private AuthManagerImp authManager;

    static int bugFix = -1;

    String stringDate;
    // RequestParam gets "file", thats why we need to specify the right name (e.g "file") in .htmls
    @RequestMapping(value = "/upload", method = RequestMethod.POST)

    public @ResponseBody FileMeta  upload(@RequestParam("file") MultipartFile file) {

        FileMeta fileMeta;
        Plan plan;
        java.util.Date date;
        SubPlan subPlan;
        if (!file.isEmpty()) {
            fileMeta = new FileMeta();
            fileMeta.setFileSize(file.getSize() / 1024 + " Kb");
            fileMeta.setFileType(file.getContentType());

            try {
                fileMeta.setBytes(file.getBytes());
                String fileExtension = fileMeta.getFileType().split("/")[1];
                BufferedOutputStream stream =
                        new BufferedOutputStream(new FileOutputStream(new File(fileMeta.getFileId())));
                stream.write(fileMeta.getBytes());
                stream.close();
                
                fileMetaService.add(fileMeta);
                logger.setLevel(Level.DEBUG);
                 logger.debug(fileMetaService.get(fileMeta.getFileId()).toString());
                files.add(fileMeta);
//                User user = getUser(username) // Тут вот из аутентификации тип получили юзера
//                user = new User("testUser","testPassword");// test
//                userService.save(user); // test
//                | | | | | |
//                ▼ ▼ ▼ ▼ ▼ ▼
                String username = authManager.getUserContext().getUsername();
                Long user_id = schedulerManager.getUserID(username);
                logger.debug(String.format("filecontroller + username  = %s",username));
                logger.debug(String.format("filecontroller + user_id  = %s",user_id));
                date = new java.util.Date();//test
                stringDate = getStringDate(date);//test

                plan = new Plan(schedulerManager.getUserID(username), stringDate, true);//test
//                schedulerManager.save(plan);

                if (schedulerManager.getPlanID(true,user_id) == null) {
                    schedulerManager.save(plan); // test
                }
                Long plan_id = schedulerManager.getPlanID(true,user_id);
//                ▲ ▲ ▲ ▲ ▲ ▲
//                | | | | | |
//                __________
//                Этот кусок выполнить только один раз, когда пользователь в первый раз заходит авторизованный.
                subPlan = new SubPlan(plan_id,"noinfo",fileMeta.getFileId());
                schedulerManager.save(subPlan);
                bugFix++;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return files.get(bugFix);
    }
    @RequestMapping(value = "/delete/{value}", method = RequestMethod.DELETE)
    public @ResponseBody LinkedList<FileMeta> delete(@PathVariable String value) {
        logger.debug(value);
        if (fileMetaService.get(value)!=null){
            logger.debug("File existed and now removed");
        }
        fileMetaService.remove(value);
        String username = authManager.getUserContext().getUsername();
        Long user_id = schedulerManager.getUserID(username);
        Long plan_id = schedulerManager.getPlanID(true,user_id);
        System.out.println("we're going to remove "+value);
        schedulerManager.deleteSubPlan(value,plan_id);
        Iterator<FileMeta> i = files.iterator();
        while (i.hasNext()){
            if (i.next().getFileId().equals(value)){
                i.remove();
            }
        }
        return files;
    }
    private String getStringDate(java.util.Date date){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return simpleDateFormat.format(date);
    }
}
