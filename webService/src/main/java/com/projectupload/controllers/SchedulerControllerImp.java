package com.projectupload.controllers;

import com.mongodb.io.ByteBufferInputStream;
import com.projectupload.TestForRabbitMQ;
import com.projectupload.auth.AbstractAuthManager;
import com.projectupload.auth.AuthManagerImp;
import com.projectupload.model.SubPlan;
import com.projectupload.scheduler.impl.QueueImp;
import com.projectupload.scheduler.impl.SchedulerManagerImp;
import com.projectupload.services.FileMetaService;
import com.projectupload.services.impl.FileMetaServiceImpl;
import com.projectupload.services.impl.PlanServiceImpl;
import com.projectupload.services.impl.SubPlanServiceImpl;
import com.projectupload.services.impl.UserServiceImpl;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by woqpw on 12.05.15.
 */
@Controller
@RequestMapping("/Schedulercontroller")
public class SchedulerControllerImp implements SchedulerController {
    private static Logger logger = Logger.getLogger(SchedulerControllerImp.class);
    static{
        logger.setLevel(Level.DEBUG);
    }

    @Autowired
    private SchedulerManagerImp schedulerManager;
    @Autowired
    private FileMetaServiceImpl fileMetaService;

    @Autowired
    AuthManagerImp authManager;

    QueueImp queue = new QueueImp();
//    QueueImp queueReceiv = new QueueImp();
//    Должно быть известно
//    String username = "testUser";//change
//
//    TestForRabbitMQ testForRabbitMQ;
    String answer;
    @Override
    @RequestMapping(value = "/redacting", method = RequestMethod.POST)
    public void getData(@RequestParam("uuid") String uuid, @RequestParam("filter") String filter) {
//        User user = new User("testname","testpassword");
//        System.out.println(user);
        // Long user_id = schedulerManager.getUserID(username);
        Long user_id = schedulerManager.getUserID(authManager.getUserContext().getUsername());
        logger.debug(String.format("current user has %d id",user_id));
        logger.debug(String.format("username %s",authManager.getUserContext().getUsername()));
        Long plan_id = schedulerManager.getPlanID(true,user_id);
        logger.debug(String.format("plan_id %s",plan_id));
        System.out.println(uuid+" "+filter);
        schedulerManager.updateFilter(plan_id,uuid,filter);
        try {
            answer = null;
            logger.debug(String.format("i send %s",uuid));
            queue.send(uuid);
            System.out.println(answer);
            answer = queue.catchFiles();
            logger.debug(String.format("get answer like this %s",answer));
            schedulerManager.updateUUID(answer,uuid);
            logger.debug(String.format("plan_id %s",plan_id));
            schedulerManager.closeSubPlan(plan_id,uuid);
            logger.debug(String.format("Answer %s",answer));
            while(answer==null) {
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Override
    @RequestMapping(value = "/closesession",method = RequestMethod.POST,produces="text/plain")
    @ResponseStatus(value= HttpStatus.OK)
    @ResponseBody
    public void closeOperation(HttpServletRequest request,HttpServletResponse response){
        logger.debug(String.format("closesession"));
        Long user_id = schedulerManager.getUserID(authManager.getUserContext().getUsername());
        Long plan_id = schedulerManager.getPlanID(true,user_id);
        boolean test = schedulerManager.checkForClose(plan_id);
        String zipName = authManager.getUserContext().getUsername()+".zip";
        if(test){
            createZip(plan_id);

        try {
            response.getWriter().println(zipName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        }
    }
    @Override
    @RequestMapping(value = "/download/{value}",method = RequestMethod.GET)
    public void downloadZip(@PathVariable("value")String filename,HttpServletResponse response){
        try{
            logger.debug(String.format("in downloadzip with name %s",filename));
            File newFile = new File(filename+".zip");
            FileInputStream inputStream = new FileInputStream(newFile);
            System.out.println(filename);
            org.apache.commons.io.IOUtils.copy(inputStream,response.getOutputStream());
            response.flushBuffer();
            response.setContentType("application/zip");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void createZip(Long plan_id){
        logger.debug("create zip");
        List<SubPlan> listSubPlan = schedulerManager.getAllByPlanID(plan_id);
        logger.debug(String.format("num = %d",listSubPlan.size()));
        ZipEntry zipEntry;
        try{
            ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(authManager.getUserContext().getUsername()+".zip"));
            for(SubPlan sp:listSubPlan) {
                zipEntry = new ZipEntry(sp.getUUIDbefore()+"."+fileMetaService.getExtension(sp.getUUIDbefore()));
                zipOutputStream.putNextEntry(zipEntry);
                zipOutputStream.write(fileMetaService.get(sp.getUUIDafter()).getBytes());
            }
            zipOutputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}