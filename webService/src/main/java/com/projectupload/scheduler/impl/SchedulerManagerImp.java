package com.projectupload.scheduler.impl;

import com.projectupload.model.Plan;
import com.projectupload.model.SubPlan;
import com.projectupload.model.User;
import com.projectupload.scheduler.AbstractSchedulerManager;
import com.projectupload.services.impl.PlanServiceImpl;
import com.projectupload.services.impl.SubPlanServiceImpl;
import com.projectupload.services.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by woqpw on 12.05.15.
 */
@Service
public class SchedulerManagerImp extends AbstractSchedulerManager {
//    TODO загрузчик в очередь к rabbitMQ
//    TODO использование Hibernate для связи с mysql?
    @Autowired
    private SubPlanServiceImpl subPlanService;
    @Autowired
    private PlanServiceImpl planService;
    @Autowired
    private UserServiceImpl userService;
    @Override
    public int getOpenPlan(int userId) {
        return 0;
    }
    @Override
    public int addToPlan(int idPlan, UUID uuid, String status, String filter) {
        return 0;
    }

// working with table User
    @Override
    public void save(User user){
        userService.save(user);
    }
    @Override
    public Long getUserID(String username){
        return userService.getUserID(username);
    }


// working with table Plan
    @Override
    public void save(Plan plan){
        planService.save(plan);
    }
    @Override
    public Long getPlanID(String stringDate){
        return planService.getPlanID(stringDate);
    }
    @Override
    public Long getPlanID(boolean status, Long user_id){
        return planService.getPlanID(status,user_id);
    }
    @Override
    public void closePlan(boolean newStatus, boolean oldStatus){
        planService.closePlan(newStatus, oldStatus);
    }


// working with table SubPlan
    @Override
    public void save(SubPlan subPlan){
        subPlanService.save(subPlan);
    }
    @Override
    public Long getSubPlanID(String uuid){
        return subPlanService.getSubPlanID(uuid);
    }
    @Override
    public void closeSubPlan(Long plan_id,String uuid){
        subPlanService.closeSubPlan(false,uuid,plan_id);
    }
    @Override
    public void updateUUID(String newUUID,String oldUUID){
        subPlanService.updateUUID(newUUID, oldUUID);
    }
    @Override
    public void deleteSubPlan(String uuid,Long plan_id){
        subPlanService.deleteSubPlan(uuid,plan_id);
    }
    @Override
    public boolean checkForClose(Long plan_id) {
        List<SubPlan> list = subPlanService.findAllByPlanID(plan_id);
        System.out.println("LIST SIZE IS "+list.size());
        for(SubPlan sp:list){
            System.out.println(sp.getUUIDbefore());
            if(!sp.getStatus()){
                System.out.println("close");
            } else return false;
        }
        return true;
    }
    @Override
    public List<SubPlan> getAllByPlanID(Long plan_id){
        List<SubPlan> list = subPlanService.findAllByPlanID(plan_id);
        return list;
    }

    @Override
    public int sendToFilter(UUID uuid, int idSubPlan, String filter) {
        return 0;
    }
    @Override
    public void updateFilter(Long plan_id,String uuid, String filter){
        subPlanService.updateFilter(plan_id,uuid,filter);
    }

}
