package com.projectupload.scheduler.impl;

import com.projectupload.scheduler.Queue;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by woqpw on 12.05.15.
 */
public class QueueImp implements Queue {
    private static Logger logger = Logger.getLogger(QueueImp.class);
    static {
        logger.setLevel(Level.DEBUG);
    }

    private final static String QUEUENAME = "Loader";
    private final static String BACKQUEUENAME = "BackLoader";
    private static ConnectionFactory connectionFactory;
    private static Connection connection;
    private static Channel channel;
    private static QueueingConsumer queueingConsumer;
    private String answer;
    @Override
    public void send(String msg) throws IOException{
        connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("localhost");
        connection = connectionFactory.newConnection();
        channel = connection.createChannel();
        channel.queueDeclare(QUEUENAME,false,false,false,null);
        channel.basicPublish("", QUEUENAME, null, msg.getBytes());
//        TODO приём файла в Filter
    }
    final String[] answerA = {"notToday"};
    @Override
    public String recv(QueueingConsumer queueingConsumer) {
        answerA[0] = "notToday";
        ScheduledExecutorService exec = Executors.newSingleThreadScheduledExecutor();
        final QueueingConsumer queueingConsumer1 = queueingConsumer;
        while (answerA[0].equals("notToday")) {
            exec.submit(new Runnable() {
                @Override
                public void run() {
                    try {
                        QueueingConsumer.Delivery delivery = queueingConsumer1.nextDelivery();
                        logger.debug(String.format("delivered %s",new String(delivery.getBody())));
                        answerA[0] = new String(delivery.getBody());
                        if (!answerA[0].equals("notToday"))
                            Thread.currentThread().interrupt();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
            if (!answerA[0].equals("notToday"))
                return answerA[0];
        }
        return answerA[0];
    }

    public String catchFiles(){
        try {
            answer = null;
            channel = connection.createChannel();
            channel.queueDeclare(BACKQUEUENAME, false, false, false, null);
            queueingConsumer = new QueueingConsumer(channel);
            channel.basicConsume(BACKQUEUENAME, true, queueingConsumer);
            answer = recv(queueingConsumer);
            logger.debug(String.format("in catchFiles return %s",answer));
            return answer;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
