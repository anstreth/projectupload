package com.projectupload.scheduler;

import com.rabbitmq.client.QueueingConsumer;

import java.io.IOException;

/**
 * Created by maksim on 10.05.15.
 */
public interface Queue {
    void send(String msg) throws IOException;
    String recv(QueueingConsumer queueingConsumer);
}