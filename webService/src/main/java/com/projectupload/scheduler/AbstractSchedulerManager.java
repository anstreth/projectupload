package com.projectupload.scheduler;

import com.projectupload.model.SubPlan;

/**
 * Created by maksim on 10.05.15.
 */
public abstract class AbstractSchedulerManager implements SchedulerManager {

    protected Queue onProcessing;
    protected Queue sinceProcessing;
    //TODO тут мог быть поток, если бы мы не узнали про Spring Task, но потом узнали о JMS

    public AbstractSchedulerManager(Queue onProcessing, Queue sinceProcessing) {
        this.onProcessing = onProcessing;
        this.sinceProcessing = sinceProcessing;
    }

    public AbstractSchedulerManager() {

    }

//    public String recvFromFilter() {
//        return this.sinceProcessing.recv();
//    }

    public Queue getOnProcessing() {
        return onProcessing;
    }

    public void setOnProcessing(Queue onProcessing) {
        this.onProcessing = onProcessing;
    }

    public Queue getSinceProcessing() {
        return sinceProcessing;
    }

    public void setSinceProcessing(Queue sinceProcessing) {
        this.sinceProcessing = sinceProcessing;
    }
}
