package com.projectupload.scheduler;

import com.projectupload.model.Plan;
import com.projectupload.model.SubPlan;
import com.projectupload.model.User;

import java.util.List;
import java.util.UUID;

/**
 * Created by maksim on 10.05.15.
 */
public interface SchedulerManager {
    int getOpenPlan(int userId);

    int addToPlan(int idPlan, UUID uuid, String status, String filter);

    int sendToFilter(UUID uuid, int idSubPlan, String filter);
    void save (SubPlan subPlan);
    void save(User user);
    void save(Plan plan);
    Long getSubPlanID(String uuid);
    Long getPlanID(String stringDate);
    Long getPlanID(boolean status,Long user_id);
    void closePlan(boolean newStatus,boolean oldStatus);
    Long getUserID(String username);
    void updateFilter(Long plan_id,String uuid,String filter);
    void closeSubPlan(Long plan_id,String uuid);
    void updateUUID(String newUUID,String oldUUID);
    void deleteSubPlan(String uuid,Long plan_id);
    boolean checkForClose(Long plan_id);
    List<SubPlan> getAllByPlanID(Long plan_id);
}
