package com.projectupload.repositories;

import com.projectupload.model.Plan;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.query.Param;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by woqpw on 20.05.15.
 */
@Repository
public interface PlanRepository extends CrudRepository<Plan,Long> {
    @Query("select p.id from Plan p where p.stringDate= :date")
    Long getIdByDate(@Param("date") String stringDate);
    @Query("select p.id from Plan p where p.status= :status and p.user_id = :user_id")
    Long getIdByStatus(@Param("status") boolean status,@Param("user_id") Long user_id);
    @Modifying
    @Query("update Plan p set p.status= :newStatus where p.status= :oldStatus")
    void closePlan(@Param("newStatus")boolean newStatus,@Param("oldStatus")boolean oldStatus);
}
