package com.projectupload.repositories;

import com.projectupload.model.SubPlan;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by woqpw on 20.05.15.
 */
@Repository
@Transactional
public interface SubPlanRepository extends CrudRepository<SubPlan,Long> {
    @Modifying
    @Query("update SubPlan sp set sp.filter = :filter where sp.UUIDbefore= :uuid and sp.plan_id= :plan_id")
    void updateFilterByUuid(@Param("plan_id") Long plan_id,@Param("uuid") String uuid, @Param("filter") String filter);
    @Query ("select sp.id from SubPlan sp where sp.UUIDbefore= :uuid")
    Long getIdByUuid(@Param("uuid") String uuid);
    @Modifying
    @Query("update SubPlan sp set sp.status = :status where sp.UUIDbefore= :uuid and sp.plan_id= :plan_id")
    void closeSubPlan(@Param("status") boolean status,@Param("uuid") String uuid,@Param("plan_id")Long plan_id);
    @Modifying
    @Query("update SubPlan sp set sp.UUIDafter= :newUUID where sp.UUIDbefore= :oldUUID")
    void updateUUID(@Param("newUUID")String newUUID,@Param("oldUUID")String oldUUID);
    @Modifying
    @Query("delete from SubPlan sp where sp.UUIDbefore= :uuid or sp.UUIDafter= :uuid and sp.plan_id= :plan_id")
    void deleteSubPlan(@Param("uuid")String uuid,@Param("plan_id")Long plan_id);
    @Query("select sp from SubPlan sp where sp.plan_id= :plan_id")
    List<SubPlan> checkByPlanID(@Param("plan_id")Long plan_id);
    
}