package com.projectupload.repositories;

import com.projectupload.model.User;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by woqpw on 17.05.15.
 */
@Repository
public interface UserRepository extends CrudRepository<User,Long> {
    @Query("select u.id from User u where u.username = :username")
    Long getIdByUsername(@Param("username") String username);

    public User findByUsername(String userName);
}
