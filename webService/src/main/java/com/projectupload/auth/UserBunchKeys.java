package com.projectupload.auth;

/**
 * Created by maksim on 09.05.15.
 */
public interface UserBunchKeys {
    void addKey(String user, String signature, String token);

    /**
     * @param user
     * @param signature некоторый признак, который позволит выявить нужный ключ среди можества ключей одного пользователя.
     *  я так полагаю, что это может быть просто айди
     */
    void removeKey(String user, String signature);

    void removeKey(String token);

    void removeAllKey(String user);

    boolean keyInBunch(String token);
}
