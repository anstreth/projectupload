package com.projectupload.auth;

import com.projectupload.auth.exception.InvalidPassword;
import com.projectupload.auth.exception.InvalidToken;
import com.projectupload.auth.exception.InvalidUser;

/**
 * Created by maksim on 10.05.15.
 */
public interface AuthManager<User> {
    /**
     * Аутентификация пользователя, получаем данные из userDataAuth;
     *
     * @param userName
     * @param userPassword
     * @return Прошла успешно true, не успешно, false.
     */
    //TODO Добавить исключения, при случаях, что пользователь не найден, не верный пароль.

    boolean authentication(String userName, String userPassword) throws InvalidUser, InvalidPassword;

    /**
     * Аутентификация токена.
     *
     * @param token
     * @return если такой токен верен, то true, если нет то false.
     */
    boolean authentication(String token) throws InvalidToken;

    /**
     * Авторизирует и возвращает токен.
     *
     * @param userName
     * @param userPassword
     * @return
     */
    String authorization(String userName, String userPassword);

    /**
     * Возвращает данные, о текцщем клиенте.
     *
     * @return
     */

    <User extends AbstractUser> User getUserContext();
}
