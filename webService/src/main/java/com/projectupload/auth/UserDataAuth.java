package com.projectupload.auth;

/**
 * Created by maksim on 09.05.15.
 */
public interface UserDataAuth<User> {
    /**
     * Возвращает пользователя
     *
     * @param userName
     * @return
     */
    public User getUser(String userName);
}
