package com.projectupload.auth;

import com.projectupload.auth.exception.InvalidPassword;
import com.projectupload.auth.exception.InvalidUser;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import com.projectupload.model.User;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.Cookie;
import java.util.UUID;

/**
 * Created by roman on 24.05.15.
 */
public class AuthManagerImp  implements AuthManager<User>{
    @Autowired
    private UserDataProvider userDataProvider;

    private Logger logger = Logger.getLogger(AuthManagerImp.class);

    public String getTokenSessionId(String token){
        try {
            return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().getId();
        } catch (SignatureException e) {
            return null;
        }
    }

    @Override
    public boolean authentication  (String userName, String userPassword) throws InvalidUser

    {
        User user = userDataProvider.getUser(userName);
        if (user == null) {
            //throw new InvalidUser();
            return false;
        }
//
//        if (!user.getPassword().equals(userPassword))
//            throw new InvalidPassword();

        return user.getPassword().equals(userPassword);
    }

    private final String secretKey = "secret";

    @Override
    public boolean authentication(String token) {
        //logger.debug(Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getSignature());
        try {
            Jwts.parser().setSigningKey(secretKey).parse(token);
        } catch (SignatureException e) {
            return false;
        }
        return true;
    }

    @Override
    public String authorization(String userName, String userPassword) {
        // JWT generation goes here i guess

        if (authentication(userName, userPassword) == false){
            throw new InvalidPassword();
        }

        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
        JwtBuilder jwtBuilder = Jwts.builder().setSubject(userName).setId(UUID.randomUUID().toString()).signWith(signatureAlgorithm, secretKey);
        logger.debug(jwtBuilder.compact());

        // if token was generated it should be stored in reddis


        return jwtBuilder.compact();
    }

    @Override
    public User getUserContext() {
        Cookie[] cookies = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getCookies();
        if (cookies == null){
            logger.debug("No cookies, user is not authorized");
            return null;
        }

        Cookie tokenCookie = null;
        for (Cookie c: cookies) {
            if (c.getName().equals("token")) {
                logger.debug("found token cookie!");
                tokenCookie = c;
                break;
            }
        }

        if (tokenCookie == null){
            logger.debug("No token cookie...");
            return null;
        }

        String username = null;

        try {
            username = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(tokenCookie.getValue()).getBody().getSubject();
        } catch (SignatureException e) {
            logger.debug("found token is not valid!");
            throw e;
        }

        logger.debug("retuning user with username " + username);
        return userDataProvider.getUser(username);
    }
}
