package com.projectupload.auth;

import com.projectupload.model.TokenLoad;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Created by roman on 25.05.15.
 */
public class UserBunchKeysImp implements UserBunchKeys {
    @Autowired
    RedisTemplate redisTemplate;

    @Resource(name = "redisTemplate")
    HashOperations hashOperations;

    public UserBunchKeysImp(){
    }

    private static Logger logger = Logger.getLogger(UserBunchKeysImp.class);
    static {
        logger.setLevel(Level.DEBUG);
    }

    public static final String COLLECTION_NAME = "tokens";

    @Override
    public void addKey(String user, String signature, String token) {
        logger.debug("adding Key to Redis!");

        TokenLoad tokenLoad = new TokenLoad();
        tokenLoad.setSessionId(signature);
        tokenLoad.setUser(user);

        hashOperations.put(COLLECTION_NAME, Long.toString(token.hashCode()), tokenLoad);
        logger.debug(hashOperations.get(COLLECTION_NAME,  Long.toString(token.hashCode())));
        // logger.debug(" " + redisTemplate.opsForHash().get("foo", "bar"));
    }

    @Override
    public void removeKey(String user, String sessionId) {
        Map<String, String> tokensMap = hashOperations.entries(COLLECTION_NAME);
        Set<String> tokensSet = tokensMap.keySet();
        for (String key: tokensSet) {
            if (tokensMap.get(key).split("$")[0].equals(user) && tokensMap.get(key).split("$")[1].equals(sessionId)){
                logger.debug("token found and deleted");
                hashOperations.delete(COLLECTION_NAME, key);
                break;
            }
        }

        logger.debug("token not found!");
    }

    @Override
    public void removeKey(String token) {
        if (keyInBunch(token)){
            logger.debug("token deleted ");
            hashOperations.delete(COLLECTION_NAME,  Long.toString(token.hashCode()));
        } else {
            logger.debug("I can't delete unregistred token");
        }
    }

    @Override
    public void removeAllKey(String user) {

        Set<String> tokensSet = hashOperations.keys(COLLECTION_NAME);
        logger.debug(tokensSet.toString());
        Iterator iterator = tokensSet.iterator();
        while (iterator.hasNext()) {
            Object key = iterator.next();
            if (hashOperations.get(COLLECTION_NAME, key) instanceof TokenLoad){
                if (((TokenLoad) hashOperations.get(COLLECTION_NAME, key)).getUser().equals(user)){
                    hashOperations.delete(COLLECTION_NAME, key);
                }
            }
        }
    }

    @Override
    public boolean keyInBunch(String token) {
        logger.debug("token hashcode is " + token.hashCode());
        hashOperations.get(COLLECTION_NAME, Long.toString(token.hashCode()));
        if (hashOperations.get(COLLECTION_NAME, Long.toString(token.hashCode()))!=null){
            logger.debug("key is in bunch");
            return true;
        }

        logger.debug("key is not in bunch!");
        return false;
    }
}
