package com.projectupload.auth.jwt;

/**
 * Created by maksim on 09.05.15.
 */
public interface TokenCreator {
    String createToken(String msg, String key);
}
