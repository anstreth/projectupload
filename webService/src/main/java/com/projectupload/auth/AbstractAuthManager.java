package com.projectupload.auth;


/**
 * Created by maksim on 09.05.15.
 * Описание: Осуществляет аутентификацию и авторизацию пользователя.
 */

public abstract class AbstractAuthManager<User> implements AuthManager {

    /**
     * Объект, который предоставляет методы, для работы сессиями авторизированных пользователей.
     */
    protected UserBunchKeys userBunchKeys;
    /**
     * Объект, который предоставит информацию о конкретном пользователе, для аундификации.
     */
    protected UserDataAuth userDataAuth;


    public UserBunchKeys getUserBunchKeys() throws RuntimeException{
        return userBunchKeys;
    }

    public void setUserBunchKeys(UserBunchKeys userBunchKeys) {
        this.userBunchKeys = userBunchKeys;
    }

    public UserDataAuth getUserDataAuth() {
        return userDataAuth;
    }

    public void setUserDataAuth(UserDataAuth userDataAuth) {
        this.userDataAuth = userDataAuth;
    }
}
