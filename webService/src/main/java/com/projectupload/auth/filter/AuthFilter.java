package com.projectupload.auth.filter;

import com.projectupload.auth.AuthManagerImp;
import com.projectupload.auth.UserBunchKeys;
import com.projectupload.auth.UserDataAuth;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.Filter;
import javax.servlet.http.*;
import java.io.IOException;

/**
 * Created by maksim on 09.05.15.
 */

@Component(value = "MyFilter")
public class AuthFilter implements Filter {

    @Autowired
    AuthManagerImp authManagerImp;

    @Autowired
    UserBunchKeys userBunchKeys;

    private static final Logger logger = Logger.getLogger(AuthFilter.class);
    private ServletContext context;
    private FilterConfig filterConfig;

    public void init(FilterConfig filterConfig) throws ServletException {
        logger.setLevel(Level.DEBUG);
        logger.fatal("Filter init");
        this.context = filterConfig.getServletContext();
        this.filterConfig = filterConfig;
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        logger.debug("doFilter");
        HttpServletRequest req = (HttpServletRequest) servletRequest;

        Cookie[] cookies = req.getCookies();
        if (cookies!=null) {

            // if has cookies check them

            Cookie tokenCookie = null;
            for (Cookie c: cookies){
                logger.debug(c.getPath());
                if (c.getName().equals("token")){
                    tokenCookie = c;
                    break;
                }
            }

            if (tokenCookie == null) {
                logger.debug("no token cookie found");
                if (((HttpServletRequest)servletRequest).getServletPath().contains("login.html")){
                    logger.debug("it's already loging page");
                    filterChain.doFilter(servletRequest, servletResponse);
                } else {
                    ((HttpServletResponse) servletResponse).sendRedirect(((HttpServletRequest) servletRequest).getContextPath() + "/login.html");
                    return;
                }
            } else {
                boolean tokenIsValid = false;
                logger.debug("checking token");

                try {
                    // if token in bunch and token is valid
                    tokenIsValid = authManagerImp.authentication(tokenCookie.getValue()) && userBunchKeys.keyInBunch(tokenCookie.getValue());
                } catch (io.jsonwebtoken.MalformedJwtException e){
                    logger.debug(e.getMessage());
                }

                if (tokenIsValid){
                    logger.debug("token is valid!");
                    if (req.getServletPath().contains("login.html")){
                        logger.debug("attempt to login while user is already logged in; redirecting to upload page...");
                        ((HttpServletResponse) servletResponse).sendRedirect(((HttpServletRequest) servletRequest).getContextPath() + "/");
                        return;
                    }
                    filterChain.doFilter(servletRequest, servletResponse);
                } else if (((HttpServletRequest) servletRequest).getServletPath().contains("login.html")) {
                    logger.debug("it's already login page");
                    filterChain.doFilter(servletRequest, servletResponse);
                } else {
                    logger.debug("token is invalid! Redirecting to login form...");
                    ((HttpServletResponse) servletResponse).sendRedirect(((HttpServletRequest) servletRequest).getContextPath() + "/login.html");
                    return;
                }
                //cookie validation
            }

//            servletResponse.setContentType("text/html");
//            ((HttpServletResponse) servletResponse).setStatus(((HttpServletResponse) servletResponse).SC_MOVED_TEMPORARILY);
//            ((HttpServletResponse) servletResponse).setHeader("Location", "login.html");
            //((HttpServletResponse)servletResponse).sendRedirect(context.getContextPath() + "/login.html");

        } else {
            if (req.getServletPath().contains("login.html")){
                logger.debug("no cookies found, but you already on the login page!");
                filterChain.doFilter(servletRequest, servletResponse);
            } else {
                logger.debug("no cookies found! Redirecting to login form...");
                ((HttpServletResponse) servletResponse).sendRedirect(((HttpServletRequest) servletRequest).getContextPath() + "/login.html");
            }
        }
    }

    public void destroy() {

    }
}
