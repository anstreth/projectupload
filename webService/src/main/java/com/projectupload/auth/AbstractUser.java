package com.projectupload.auth;

/**
 * Created by maksim on 10.05.15.
 */
public interface AbstractUser {
    String getUsername();
    String getPassword();
    Long getId();
}
