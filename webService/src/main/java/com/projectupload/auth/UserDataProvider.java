package com.projectupload.auth;

import com.projectupload.model.User;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import com.projectupload.repositories.UserRepository;

import javax.annotation.Resource;

/**
 * Created by roman on 21.05.15.
 */

@Resource(name = "UserDataProvider")
public class UserDataProvider implements UserDataAuth<User>{
    @Autowired
    private UserRepository userRepository;


    @Override
    public User getUser(String userName) {
        return userRepository.findByUsername(userName);
    }
}
