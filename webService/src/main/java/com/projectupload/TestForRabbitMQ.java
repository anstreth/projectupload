package com.projectupload;

import com.projectupload.scheduler.impl.SchedulerManagerImp;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by woqpw on 25.05.15.
 */
public class TestForRabbitMQ {
    private String QUEUENAME = "Loader";
    private String BACKQUEUENAME = "BackLoader";
    private ConnectionFactory connectionFactory;
    private Connection connection;
    private Channel channel;
    QueueingConsumer queueingConsumer;
    @Autowired
    private SchedulerManagerImp schedulerManager;
    public void runFilterMain(){
        ScheduledExecutorService exec = Executors.newSingleThreadScheduledExecutor();
        exec.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                System.out.println("runFilter");
                runFilter();
            }
        }, 0, 1, TimeUnit.SECONDS);
    }
    public TestForRabbitMQ(){
        connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("localhost");
        try{
            connection = connectionFactory.newConnection();
            channel = connection.createChannel();
            channel.queueDeclare(QUEUENAME,false,false,false,null);
            queueingConsumer = new QueueingConsumer(channel);
            channel.basicConsume(QUEUENAME,true,queueingConsumer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void runFilter(){
//        ConnectionFactory connectionFactory = new ConnectionFactory();
//        connectionFactory.setHost("localhost");
        try {
//            Connection connection = connectionFactory.newConnection();
//            System.out.println("new connection");
//            Channel channel = connection.createChannel();
//            System.out.println("new channel");
//            channel.queueDeclare(QUEUENAME,false,false,false,null);
//            System.out.println("q. declared");
//            System.out.println(" [*] Waiting for messages. To exit press CTRL+C");
//            QueueingConsumer queueingConsumer = new QueueingConsumer(channel);
//            channel.basicConsume(QUEUENAME,true,queueingConsumer);
            while(true){
                QueueingConsumer.Delivery delivery = queueingConsumer.nextDelivery();
                String message = new String(delivery.getBody());
                System.out.println("get uuid "+message);
                Connection connection1 = connectionFactory.newConnection();
                Channel channel1 = connection1.createChannel();
                channel1.queueDeclare(BACKQUEUENAME,false,false,false,null);
                System.out.println("new q declared");
                String newMessage = "1"+message;
                System.out.println(newMessage+" is new Message");
//                schedulerManager.updateUUID(newMessage,message);
                System.out.println("updated UUID");
//                Thread.currentThread().sleep(5000);
                channel.basicPublish("",BACKQUEUENAME,null,newMessage.getBytes());
                System.out.println("newUUID sended");
                break;
//                connection1.close();
//                channel1.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
