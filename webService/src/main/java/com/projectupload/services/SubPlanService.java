package com.projectupload.services;

import com.projectupload.model.SubPlan;

import java.util.List;

/**
 * Created by woqpw on 12.05.15.
 */
public interface SubPlanService {
    void save(SubPlan subPlan);
    void updateFilter(Long plan_id,String uuid,String filter);
    Long getSubPlanID(String uuid);
    void closeSubPlan(boolean status,String uuid, Long plan_id);
    void updateUUID(String newUUID,String oldUUID);
    void deleteSubPlan(String uuid,Long plan_id);
    List<SubPlan> findAllByPlanID(Long plan_id);
}
