package com.projectupload.services.impl;

import com.projectupload.model.SubPlan;
import com.projectupload.repositories.SubPlanRepository;
import com.projectupload.services.SubPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by woqpw on 20.05.15.
 */
@Service
public class SubPlanServiceImpl implements SubPlanService {
    @Qualifier("subPlanRepository")
    @Autowired
    private SubPlanRepository subPlanRepository;
    @Override
    public void save(SubPlan subPlan) {
        subPlanRepository.save(subPlan);
    }
    @Override
    public void updateFilter(Long plan_id,String uuid, String filter){
        subPlanRepository.updateFilterByUuid(plan_id,uuid,filter);
    }
    @Override
    public Long getSubPlanID(String uuid){
        return null;
    }
    @Override
    public void closeSubPlan(boolean status,String uuid,Long plan_id){
        subPlanRepository.closeSubPlan(status,uuid,plan_id);
    }
    @Override
    public void updateUUID(String newUUID,String oldUUID){
        subPlanRepository.updateUUID(newUUID, oldUUID);
    }
    @Override
    public void deleteSubPlan(String uuid,Long plan_id){
        subPlanRepository.deleteSubPlan(uuid,plan_id);
    }
    @Override
    public List<SubPlan> findAllByPlanID(Long plan_id){
        return subPlanRepository.checkByPlanID(plan_id);
    }
    private static <E> List<E> toList(Iterable<E> iterable){
        if(iterable instanceof List){
            return(List<E>)iterable;
        }
        ArrayList<E> list = new ArrayList<E>();
        if(iterable!=null){
            for(E e:iterable){
                list.add(e);
            }
        }
        return list;
    }
}
