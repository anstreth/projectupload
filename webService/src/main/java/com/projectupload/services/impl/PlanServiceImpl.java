package com.projectupload.services.impl;

import com.projectupload.model.Plan;
import com.projectupload.repositories.PlanRepository;
import com.projectupload.services.PlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * Created by woqpw on 20.05.15.
 */
@Service
public class PlanServiceImpl implements PlanService {
    @Qualifier("planRepository")
    @Autowired
    private PlanRepository planRepository;
    @Override
    public void save(Plan plan) {
        planRepository.save(plan);
    }
    @Override
    public Long getPlanID(String stringDate){
        return planRepository.getIdByDate(stringDate);
    }
    @Override
    public Long getPlanID(boolean status,Long user_id){
        return planRepository.getIdByStatus(status,user_id);
    }
    @Override
    public void closePlan(boolean newStatus, boolean oldStatus){
        planRepository.closePlan(newStatus, oldStatus);
    }
}
