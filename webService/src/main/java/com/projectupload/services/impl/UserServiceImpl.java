package com.projectupload.services.impl;

import com.projectupload.model.User;
import com.projectupload.repositories.UserRepository;
import com.projectupload.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * Created by woqpw on 20.05.15.
 */
@Service
public class UserServiceImpl implements UserService {
    @Qualifier("userRepository")
    @Autowired
    private UserRepository userRepository;
    @Override
    public void save(User user){
        userRepository.save(user);
    }
    @Override
    public Long getUserID(String username){
        return userRepository.getIdByUsername(username);
    }
}
