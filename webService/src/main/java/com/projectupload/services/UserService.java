package com.projectupload.services;

import com.projectupload.model.User;

/**
 * Created by woqpw on 17.05.15.
 */
public interface UserService {
    void save(User user);
    Long getUserID(String username);
}
