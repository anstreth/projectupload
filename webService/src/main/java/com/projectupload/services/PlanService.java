package com.projectupload.services;

import com.projectupload.model.Plan;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by woqpw on 12.05.15.
 */
public interface PlanService {
    void save(Plan plan);
    Long getPlanID(String stringDate);
    Long getPlanID(boolean status,Long user_id);
    void closePlan(boolean newStatus,boolean oldStatus);
}
