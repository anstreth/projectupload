package com.projectupload.services;

import com.projectupload.dao.FileMetaDao;
import com.projectupload.model.FileMeta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by root on 07.05.15.
 */

public interface FileMetaService {
    void add(FileMeta fileMeta);
    void update(FileMeta fileMeta);
    FileMeta get(String id);
    void remove(String id);
    String getExtension(String id);
    void test();
//    List<FileMeta> getAll();
}
