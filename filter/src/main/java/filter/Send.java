package filter;

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;

/**
 * Created by artemypestretsov on 5/22/15.
 */
public class Send {
    private final static String QUEUE_NAME = "Loader";

    public static void main(String[] args) throws java.io.IOException {

        // absract from SOCKETS and so on
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();


        // declare
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);

        byte[] imageInByte;
        // send
        BufferedImage originalImage = ImageIO.read(new File("/Users/artemypestretsov/IdeaProjects/projectupload/filter/src/main/resources/slowpoke.jpg"));
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(originalImage, "jpg", baos);
        baos.flush();
        imageInByte = baos.toByteArray();
        baos.close();

        channel.basicPublish("", QUEUE_NAME, null, imageInByte);
        System.out.println(" [x] Sent '" + imageInByte + "'");


        // close
        channel.close();
        connection.close();
    }
}
