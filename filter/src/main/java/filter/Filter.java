package filter;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;
import filterengine.FilterEngine;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by artemypestretsov on 5/22/15.
 */
public class Filter {

    private ThreadPoolTaskExecutor taskExecutor;

    private final static String IN_QUEUE_NAME = "Loader";
    private final static String OUT_QUEUE_NAME = "BackLoader";
    private Connection mConnection;
    private Channel mChannel;

    public static void main(String[] args) {

        Filter filter = new Filter();


        ApplicationContext context = new ClassPathXmlApplicationContext("appContext.xml");
        filter.taskExecutor = (ThreadPoolTaskExecutor) context.getBean("taskExecutor");
        FilterEngine filterEngine = new FilterEngine();

        try {
            // load scanner
            filterEngine.pluginScanner();

            // connect to rabbitMQ INPUT QUEUE
            filter.establishConnection();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }


        QueueingConsumer consumer = new QueueingConsumer(filter.mChannel);

        try {
            filter.mChannel.queueDeclare(IN_QUEUE_NAME, false, false, false, null);
            filter.mChannel.basicConsume(IN_QUEUE_NAME, true, consumer);
            //filter.mChannel.queueDeclare(OUT_QUEUE_NAME, false, false, false, null);
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("[*] Waiting for messages. To exit press CTRL+C");


        // declare



        while (true) {
            try {
                QueueingConsumer.Delivery delivery = consumer.nextDelivery();
                //String imageUUID = new String(delivery.getBody());
                System.out.println(" [x] Received imageUUID: " + delivery.getBody());

                // TODO:
                // get filter name from PLAN table
                String filterName = "BoxBlurFilter.class";

                String imageUUID = new String(delivery.getBody());

                System.out.println(filterName);
                filter.taskExecutor.execute(new ImageProcessingTask(imageUUID, filterName, OUT_QUEUE_NAME));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void establishConnection() throws IOException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        mConnection = factory.newConnection();
        mChannel = mConnection.createChannel();
    }
}
