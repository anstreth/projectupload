package filter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.gridfs.GridFSDBFile;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import model.FileMeta;
import org.apache.commons.io.IOUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsOperations;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * Created by artemypestretsov on 5/23/15.
 */
@Component
class ImageProcessingTask implements Runnable {

    private String imageUUID;
    private String queueName;
    private String filterName;
    ApplicationContext context = new ClassPathXmlApplicationContext("appContext.xml");

    private GridFsOperations gridFsOperations = (GridFsOperations) context.getBean("gridFsTemplate");

    public void save(FileMeta fileMeta){
        // mongoOperations.save(fileMeta);
        try{
            DBObject metaData = new BasicDBObject();
            metaData.put("ext",fileMeta.getFileType());
            System.out.println("EXTENSION IS:" + fileMeta.getFileType());
            System.out.println("NEW UUID IS:" + fileMeta.getFileId());
            BufferedOutputStream stream =
                    new BufferedOutputStream(new FileOutputStream(new File(fileMeta.getFileId())));
            stream.write(fileMeta.getBytes());
            stream.close();
            InputStream inputStream = new FileInputStream(fileMeta.getFileId());
            gridFsOperations.store(inputStream,fileMeta.getFileId(),"image/png",metaData);
            inputStream.close();
        } catch (IOException e){
            System.out.println("error saving file in mongo");
        }
    }

    public FileMeta get(String id){
//        System.out.println("in get method");
        //return mongoOperations.findOne(Query.query(Criteria.where("_id").is(id)), FileMeta.class);
        // return mongoOperations.findById(id, FileMeta.class);
        // return gridFsOperations.findOne(Query.query(Criteria.where("_id").is(id)));
        FileMeta fileMeta = new FileMeta();
        GridFSDBFile gridFSDBFile = new GridFSDBFile();
        gridFSDBFile = gridFsOperations.findOne(Query.query(Criteria.where("filename").is(id)));
        InputStream inputStream = null;
        try{
            inputStream = gridFSDBFile.getInputStream();
            fileMeta.setBytes(IOUtils.toByteArray(inputStream));
            inputStream = null;
        } catch(IOException e){
            System.out.println("error getting file");
        }
        return fileMeta;
    }

    //private FileMetaServiceImpl fileMetaService = (FileMetaServiceImpl) FilterEngine.context.getBean("taskExecutor");

    public ImageProcessingTask(String imageUUID, String filterName, String queueName) {
        this.imageUUID = imageUUID;
        this.queueName = queueName;
        this.filterName = filterName;
    }

    @Override
    public void run() {
        File file = new File("plugin/.jar");

        ClassLoader loader = null;

        try {
            loader = new URLClassLoader(new URL[]{file.toURI().toURL()});

            Class myClass = null;

            myClass = loader.loadClass(filterName.substring(0, filterName.length() - ".class".length()));


            // OUTPUT QUEUE
            // ALREADY DECLARED IN FILTER CLASS
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost("localhost");

            Connection connection = factory.newConnection();
            Channel channel = connection.createChannel();

            System.out.println(imageUUID);
            byte[] imageInByte = get(imageUUID).getBytes();

            InputStream ints = new ByteArrayInputStream(imageInByte);

            BufferedImage result = ImageIO.read(ints);

            Object filter = null;

            InputStream in = new ByteArrayInputStream(imageInByte);
            BufferedImage bImageFromConvert = ImageIO.read(in);

            filter = myClass.newInstance();

            // APPLY FILTER
            Method config = null;
            Method filterMethod = null;

            for (Method method : myClass.getMethods()) {
                System.out.println(method.getName());
                if (method.isAnnotationPresent(annotation.FilterConfig.class)) {
                    config = method;
                }

                if (method.isAnnotationPresent(annotation.Filter.class)) {
                    filterMethod = method;
                }
            }

            if (config != null)
                config.invoke(filter, 50, 50, 5);
            filterMethod.invoke(filter, bImageFromConvert, result);

            // CONVERT RESULT TO BYTES
            ByteArrayOutputStream converter = new ByteArrayOutputStream();

            ImageIO.write(result, "jpg", converter);
            converter.flush();
            imageInByte = converter.toByteArray();
            converter.close();

            // load new file to mongodb
            FileMeta updateFile = new FileMeta();
            updateFile.setBytes(imageInByte);
            updateFile.setFileType("image/png");
            save(updateFile);

            // SEND RESULT

            channel.basicPublish("", queueName, null, updateFile.getFileId().getBytes());
            System.out.println("RECEIVED::FILE IS SENT");

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}