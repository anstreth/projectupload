package services.impl;

import dao.FileMetaDao;
import model.FileMeta;
import services.FileMetaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by woqpw on 21.05.15.
 */
@Service
public class FileMetaServiceImpl implements FileMetaService {
    @Autowired
    FileMetaDao fileDao;
    @Override
    public void add(FileMeta fileMeta){
        fileDao.save(fileMeta);
    }
    @Override
    public void update(FileMeta fileMeta){
        fileDao.save(fileMeta);
    }
    @Override
    public FileMeta get(String id){
        return fileDao.get(id);
    }
    /*public List<FileMeta> getAll(){
        return fileDao.getAll();
    }*/
    @Override
    public void remove(String id){
        fileDao.remove(id);
    }

    @Override
    public String getExtension(String id){
        return fileDao.getExtension(id).split("/")[1];
    }
    @Override
    public  void test(){
        System.out.println("hello");
    }
    /*@Override
    public List<FileMeta> getAll(){
        return
    }*/
}
