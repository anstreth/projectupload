package services;

import dao.FileMetaDao;
import model.FileMeta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by root on 07.05.15.
 */

public interface FileMetaService {
    void add(model.FileMeta fileMeta);
    void update(model.FileMeta fileMeta);
    model.FileMeta get(String id);
    void remove(String id);
    String getExtension(String id);
    void test();
//    List<FileMeta> getAll();
}
