package filterengine;

import annotation.FilterConfig;
import annotation.FilterParam;
import annotation.FilterPlugin;

import java.sql.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by artemypestretsov on 5/23/15.
 */


public class FilterEngine {

    private static String driverName = "com.mysql.jdbc.Driver";
    private static String baseAddress = "jdbc:mysql://localhost:3306/projectupload";
    private static String username = "root";
    private static String password = "newpassword";
    private static String tableName = "filters";

    private static Connection connection = null;

    public static Connection getConnectionToDB() {

        if (connection != null)
            return connection;

        try {
            Class.forName(driverName);
            connection = DriverManager.getConnection(baseAddress, username, password);
        } catch (SQLException e) {
            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            System.out.println("MySQL JDBC Driver problems!");
            e.printStackTrace();
        }

        return connection;
    }

    public void pluginScanner() throws IOException, ClassNotFoundException, SQLException {
//        Properties prop = new Properties();
//        InputStream input = null;
//        String filename =  "path/to/file"
//        input = getClass().getClassLoader().getResourceAsStream(filename);
//        if (input == null) {
//            System.out.println("Sorry, unable to find " + filename);
//            return;
//        }
//        prop.load(input);
//        System.out.println(prop.getProperty("plugin"));
//

        File file = new File("plugin/.jar");

        Connection connection = getConnectionToDB();
        Statement flushStatement;
        PreparedStatement insertStatement;

        // first - clear present records:

        flushStatement = connection.createStatement();
        int deletedRows = flushStatement.executeUpdate("DELETE FROM filters");
        System.out.println("Deleted " + deletedRows + " rows.");

        ClassLoader loader = new URLClassLoader(new URL[]{file.toURI().toURL()});

        List<String> classNames = new ArrayList<String>();
        ZipInputStream zip = new ZipInputStream(new FileInputStream("filter/plugin/myJar.jar"));

        int id = 0;

        for (ZipEntry entry = zip.getNextEntry(); entry != null; entry = zip.getNextEntry()) {
            if (!entry.isDirectory() && entry.getName().endsWith(".class")) {
                String className = entry.getName().replace('/', '.'); // including ".class"

                classNames.add(className.substring(0, className.length() - ".class".length()));

                System.out.println(className.substring(0, className.length() - ".class".length()));
                Class myClass = loader.loadClass(className.substring(0, className.length() - ".class".length()));

                if (myClass.isAnnotationPresent(FilterPlugin.class)) {

                    System.out.println("I found Filter class: " + className);
                    Method [] methods = myClass.getMethods();

                    boolean foundConfig = false;
                    boolean foundFilter = false;
                    boolean foundParam = false;

                    if (myClass.getAnnotation(FilterPlugin.class) instanceof FilterPlugin) {
                        System.out.println("WOW");
                        id++;
                    }

                    System.out.println("HEH:" + myClass.getAnnotation(FilterPlugin.class).toString());

                    String filterName = ((FilterPlugin)myClass.getAnnotation(FilterPlugin.class)).name();
                    String insertRow = "INSERT INTO "+ tableName +"(filtername, filterparams) " + "VALUES(?,?)";

                    insertStatement = connection.prepareStatement(insertRow);
                    insertStatement.setString(1, filterName);

                    String paramsList = "";
                    int j = 0;

                    for (Method method : methods) {
                        System.out.println("TEST" + method.toString());
                        Annotation[][] parameterAnnotations = method.getParameterAnnotations();
                        Class<?>[] paramTypes = method.getParameterTypes();

                        for (Annotation[] annotations : parameterAnnotations) {
                            for (Annotation annotation : annotations) {
                                System.out.println("HEH:" + annotation.toString());
                                if (annotation instanceof FilterParam) {
                                    foundParam = true;

                                    FilterParam myAnnotation = (FilterParam) annotation;

                                    // get param names to pass to MySQL
                                    System.out.print("name: " + myAnnotation.name() + "\t");
                                    // get param types to pass to MySQL
                                    System.out.println("type: " + paramTypes[j]);
                                    //insertRow += myAnnotation.name() + ":" + paramTypes[j++] + "$";
                                    paramsList += "$" + myAnnotation.name() + ":" + paramTypes[j++];
                                }
                                if (annotation instanceof FilterConfig) {
                                    System.out.println("CONFIG FOUND");
                                    foundConfig = true;
                                }
                            }
                        }
                    }

                    if (!foundParam) {
                        insertStatement.setNull(2, Types.VARCHAR);
                    } else {
                        insertStatement.setString(2, paramsList);
                    }

                    insertStatement.executeUpdate();
                }
            }
        }
        connection.close();
    }
}
