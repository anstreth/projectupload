package model;

import java.util.*;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

//ignore "bytes" when return json format
@JsonIgnoreProperties({"bytes"})
@Document(collection = FileMeta.COLLECTION_NAME)
public class FileMeta{

    public final static String COLLECTION_NAME = "photos";
    // maybe we'll need UUID later
    // cannot overrude fileId cause should be unique
    @Id
    private String fileId;

    private String fileSize;
    private String fileType;
    private UUID fileUUID;

    // to set the UUID
    private byte[] bytes;

    public String getFileId() {
        return fileId;
    }

    public String getFileSize() {
        return fileSize;
    }

    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        fileUUID = UUID.nameUUIDFromBytes(bytes);
        fileId = fileUUID.toString();
        this.bytes = bytes;
    }

    @JsonIgnore
    public String toString(){
        return "FileId is " + fileId + ", FileSize is " + fileSize+", FileType is "+fileType;
    }


    @JsonIgnore
    public static UUID calculateUUID(byte[] bytes) {
        return UUID.nameUUIDFromBytes(bytes);
    }
}
