package dao;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.gridfs.GridFSDBFile;


import model.FileMeta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.gridfs.GridFsOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.io.InputStream;
import java.io.FileInputStream;

import org.apache.commons.io.IOUtils;

/**
 * Created by root on 07.05.15.
 */
@Repository
public class FileMetaDao {
    @Autowired
    private GridFsOperations gridFsOperations;
    // private MongoOperations mongoOperations;
    public void save(FileMeta fileMeta){
        // mongoOperations.save(fileMeta);
        try{
            DBObject metaData = new BasicDBObject();
            metaData.put("ext",fileMeta.getFileType());
            System.out.println(fileMeta.getFileId());
            InputStream inputStream = new FileInputStream(fileMeta.getFileId());
            gridFsOperations.store(inputStream,fileMeta.getFileId(),"image/png",metaData);
            inputStream.close();
        } catch (IOException e){
            System.out.println("error saving file in mongo");
        }
    }

    public FileMeta get(String id){
//        System.out.println("in get method");
        //return mongoOperations.findOne(Query.query(Criteria.where("_id").is(id)), FileMeta.class);
        // return mongoOperations.findById(id, FileMeta.class);
        // return gridFsOperations.findOne(Query.query(Criteria.where("_id").is(id)));
        FileMeta fileMeta = new FileMeta();
        GridFSDBFile gridFSDBFile = new GridFSDBFile();
        gridFSDBFile = gridFsOperations.findOne(Query.query(Criteria.where("filename").is(id)));
        InputStream inputStream = null;
        try{
            inputStream = gridFSDBFile.getInputStream();
            fileMeta.setBytes(IOUtils.toByteArray(inputStream));
            inputStream = null;
        } catch(IOException e){
        System.out.println("error getting file");
        }
        return fileMeta;
    }
    public String getExtension(String id){
        GridFSDBFile gridFsDBFile = new GridFSDBFile();
        gridFsDBFile = (gridFsOperations.findOne(Query.query(Criteria.where("filename").is(id))));
        DBObject dbObject = gridFsDBFile.getMetaData();
        return String.valueOf(dbObject.get("ext"));
    }
    // public List<FileMeta> getAll(){return mongoOperations.findAll(FileMeta.class);}
    public void remove(String id){
        // hate this _
        // mongoOperations.remove(Query.query(Criteria.where("_id").is(id)), FileMeta.class);}
        gridFsOperations.delete(Query.query(Criteria.where("filename").is(id)));
    }
}
