package annotation;

import java.lang.annotation.*;

/**
 * Created by maksim on 13.05.15.
 */
@Documented
@Inherited
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface FilterPlugin {
    String name();
}
